# Mailflow for Rails

Mailflow for Rails allows you to integrate directly into Action Mailer in a Rails application.

## Install

Add the `mailflow-rails` gem to your Gemfile and run `bundle install` to install it.

```ruby
gem 'mailflow-rails', '~> 0.0.2'
```

## Config

Update your `config/environment/production.rb` for production and `config/environment/development.rb` for development ENV.

```ruby
# change delivery method to :mailflow
config.action_mailer.delivery_method = :mailflow
# You can set the `MAILFLOW_KEY` environment variable to include key and `MAILFLOW_HOST` to include the hostname of your Mailflow service or add them directly to `config/environment/development.rb`.
config.action_mailer.mailflow_settings = {:host => "subdomain.mailflow.cloud", :server_key => "SERVER_KEY_TOKEN"}
```

Use `deliver!` when running your tests so any exceptions are raised.

