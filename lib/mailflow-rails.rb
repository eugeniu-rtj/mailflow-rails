require 'mailflow'
require 'mailflow/rails/action_mailer_delivery_method'

if defined?(ActionMailer)
  ActionMailer::Base.add_delivery_method :mailflow, Mailflow::Rails::ActionMailerDeliveryMethod
end
