module Mailflow
  module Rails
    class ActionMailerDeliveryMethod
      attr_reader :settings

      def initialize(settings)
        @settings = settings
      end

      def deliver!(message)
        client.send_raw_message do |m|
          m.mail_from(message.from.first)
          add_recipients(m, message)
          m.data(message.to_s)
        end
      rescue StandardError => e
        # Consider logging the error or re-raising a custom error with a clearer message.
        raise "Failed to deliver message: #{e.message}"
      end

      private

        def client
          @client ||= Mailflow::Client.new(
            settings.fetch(:host, ENV['MAILFLOW_HOST']),
            settings.fetch(:server_key, ENV['MAILFLOW_KEY'])
          )
        end

        def add_recipients(m, message)
          [:to, :cc, :bcc].each do |type|
            next unless message.send(type)

            message.send(type).each do |address|
              m.rcpt_to(address)
            end
          end
        end
    end
  end
end
