require File.expand_path('../lib/mailflow/rails/version', __FILE__)

Gem::Specification.new do |s|
  s.name          = "mailflow-rails"
  s.description   = %q{Mailflow.cloud library for Rails/ActionMailer library}
  s.summary       = s.description
  s.homepage      = "https://mailflow.cloud"
  s.version       = Mailflow::Rails::VERSION
  s.files         = Dir.glob("{lib}/**/*")
  s.require_paths = ["lib"]
  s.authors       = ["Mailflow"]
  s.email         = ["support@mailflow.cloud"]
  s.licenses      = ['MIT']
  s.add_dependency "mailflow"
end
